#include <zephyr/kernel.h>
#include <zephyr/sys/util.h>
#include <zephyr/drivers/gpio.h>
#include "iod.h"
#include "iod_nvs.h"

const struct gpio_dt_spec led_dev[] = {
	GPIO_DT_SPEC_GET(DT_ALIAS(ledblue), gpios),
	GPIO_DT_SPEC_GET(DT_ALIAS(ledred), gpios),
	GPIO_DT_SPEC_GET(DT_ALIAS(ledgreen), gpios),
};

struct str_iod iod;

static void _milaniSystemPrefixSetName_startUpDefault(struct str_iod *FStrutturaIod)
{
	int Llen = sizeof(FStrutturaIod->milani_system_prefix) / sizeof(FStrutturaIod->milani_system_prefix[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		FStrutturaIod->milani_system_prefix[i] = 0;
	}
	FStrutturaIod->milani_system_prefix[0] =19817;//0x4D|| arithmetic_shift_right(0x69) // 65535; //'M' << 8 | 'i';  //correggi
	FStrutturaIod->milani_system_prefix[1] =27745;// 65535; //'l' << 8 | 'a';
	FStrutturaIod->milani_system_prefix[2] = 28265;//65535; //'n' << 8 | 'i';
	FStrutturaIod->milani_system_prefix[3] =16705;// 65535; // 0;
	//4D 69 6C 61 6E 69 41 41
}

static void _init_iod_busaddr_startUpDefault(struct str_iod *FStrutturaIod)
{
	int Llen = sizeof(FStrutturaIod->iod_busaddr) / sizeof(FStrutturaIod->iod_busaddr[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		FStrutturaIod->iod_busaddr[i] = 4;
	}
}

static void _bms_busid_startUpDefault(struct str_iod *FStrutturaIod)
{
	int Llen = sizeof(FStrutturaIod->bms_busid) / sizeof(FStrutturaIod->bms_busid[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		FStrutturaIod->bms_busid[i] = 5;
	}
}

static void _iod_config_id_startUpDefault(struct str_iod *FStrutturaIod)
{
	 int Llen = sizeof(FStrutturaIod->iod_config_id) / sizeof(FStrutturaIod->iod_config_id[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		FStrutturaIod->iod_config_id[i] = 6;
	}
}

static void _targetobj_id_startUpDefault(struct str_iod *FStrutturaIod)
{
	 int Llen = sizeof(FStrutturaIod->targetobj_id) / sizeof(FStrutturaIod->targetobj_id[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		FStrutturaIod->targetobj_id[i] = 7;
	}
}

static void _app_id_startUpDefault(struct str_iod *FStrutturaIod)
{
	 int Llen = sizeof(FStrutturaIod->app_id) / sizeof(FStrutturaIod->app_id[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		FStrutturaIod->app_id[i] = 9;
	}
}

static void _tag_id_startUpDefault(struct str_iod *FStrutturaIod)
{
	 int Llen = sizeof(FStrutturaIod->tag_id) / sizeof(FStrutturaIod->tag_id[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		FStrutturaIod->tag_id[i] = 10;
	}
}

static void _cfg_password_startUpDefault(struct str_iod *FStrutturaIod)
{
	 int Llen = sizeof(FStrutturaIod->cfg_password) / sizeof(FStrutturaIod->cfg_password[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		FStrutturaIod->cfg_password[i] = 12;
	}
}

static void _cfg_action_startUpDefault(struct str_iod *FStrutturaIod)
{
	 int Llen = sizeof(FStrutturaIod->cfg_action) / sizeof(FStrutturaIod->cfg_action[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		FStrutturaIod->cfg_action[i] = 16;
	}
}

static void _cfg_data_startUpDefault(struct str_iod *FStrutturaIod)
{
	 int Llen = sizeof(FStrutturaIod->cfg_data) / sizeof(FStrutturaIod->cfg_data[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		FStrutturaIod->cfg_data[i] = 17;
	}
}

static void _msg_type_startUpDefault(struct str_iod *FStrutturaIod)
{
	 int Llen = sizeof(FStrutturaIod->msg_type) / sizeof(FStrutturaIod->msg_type[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		FStrutturaIod->msg_type[i] = 18;
	}
}

static void _rssi_level_startUpDefault(struct str_iod *FStrutturaIod)
{
	 int Llen = sizeof(FStrutturaIod->rssi_level) / sizeof(FStrutturaIod->rssi_level[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		FStrutturaIod->rssi_level[i] = 19;
	}
}

static void _bms_reaction_startUpDefault(struct str_iod *FStrutturaIod)
{
	 int Llen = sizeof(FStrutturaIod->bms_reaction) / sizeof(FStrutturaIod->bms_reaction[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		FStrutturaIod->bms_reaction[i] = i;
	}
}

int init_str_iod_factoryDefault(struct str_iod *FStrutturaIod)
// alla partenza assegno valore a tutta la struttura in modo da non lasciare campi non inizializzati
{
	_milaniSystemPrefixSetName_startUpDefault(FStrutturaIod);
	_init_iod_busaddr_startUpDefault(FStrutturaIod);
	_bms_busid_startUpDefault(FStrutturaIod);
	_iod_config_id_startUpDefault(FStrutturaIod);
	_targetobj_id_startUpDefault(FStrutturaIod);
	_app_id_startUpDefault(FStrutturaIod);
	_tag_id_startUpDefault(FStrutturaIod);
	_cfg_password_startUpDefault(FStrutturaIod);
	_cfg_action_startUpDefault(FStrutturaIod);
	_cfg_data_startUpDefault(FStrutturaIod);
	_msg_type_startUpDefault(FStrutturaIod);
	_rssi_level_startUpDefault(FStrutturaIod);
	_bms_reaction_startUpDefault(FStrutturaIod);

	return 0;
}

int init_str_iod_LoadFromFlash(struct str_iod *Fstruttura_iod)
{
	return loadDataFromFlash();
}

int init_hw_iod(void)
{

	 int Llen = ARRAY_SIZE(led_dev);
	for (int i = 0; i < Llen; i++)
	{
		gpio_pin_configure_dt(&led_dev[i], GPIO_OUTPUT_INACTIVE);
	}

	return 0;
}
