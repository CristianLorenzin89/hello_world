#ifndef IOD_H
#define IOD_H

#include <zephyr/kernel.h>
#include <zephyr/drivers/gpio.h>





struct str_iod
{
	
	uint16_t milani_system_prefix[4];// OK TIENI
	// prefisso che identifica univocamente la Milani e l'impianto
	// 		  M  i   l  a     n  i    +   numero impianto 0- 4,294,967,295    
	//----->(4D 69 / 6C 61 / 6E 69)   +   				(FFFF FFFF)
	//--> 4 word 8 byte

	uint16_t iod_busaddr[1];// OK TIENI
	// indirizzo dell'IOD sul bus dali/modbus  deve essere noto al CMS per identificare univocamente l'IOD
	//----->  (1-32)
	//----->1 word  in previsione anche del dali

	uint16_t bms_busid[1];// OK TIENI
	// id univoco da assegnare a ciascun bus (dali/modbus) dell'impianto  (finisce nel master bus)
	//- deve essere noto al PLC che gestisce il bus
	//- deve essere noto al CMS per identificare univocamente l'IOD
	//----->  (0-65535)
	//(00 00 - FF FF)
	//--> 1 word

	uint16_t iod_config_id[1];// OK TIENI  // mac address? se basta lui siamo gia ok (todo)
	// numero random, basta che sia univoco nel range di visibilità radio di questo IOD
	//= numero random, basta che sia univoco nel range di visibilità radio di questo IOD
	//----->  (33-247)
	//(00 21 - 00 F7)
	//--> 1 byte

	uint16_t targetobj_id[2];//GEESTIONE MESSAGGI  //TIRA FUORI DA IOD STRUTT . SERVER PER HEAT MAP (TODO)
	// id univoco nell'impianto del target (quadro, posizione, etc) assegnato dal CMS
	// building /       piano      /   ROOM   /     ITEM
	// (0-255)  /      (0-255)     /  (0-255)   /     (0-255)  

	uint16_t app_id[1];// VIA, GESTIONE MESSAGI // identificatore univoco di chi si collega all'iod 
	// id univoco nell'impianto della APP sul cellulare
	//(VEDI SOPRA milani_system_prefix)
	// impianto 0- 65...    
	//(FFFF FFFF)
	//-> 1 word

	uint16_t tag_id[2];// VIA, GESTIONE MESSAGGI // CODICE UNIVOCO DEL DEVICE LIKE MAC 
	// id univoco del tag ble acquistato sul mercato
	// (FFFF FFFF)
	//-> 2 word
	// U32

	uint16_t cfg_password[4];// TIENI
	// password di sblocco per la configurazione dell'IOD
	//(FFFF FFFF FFFF FFFF)
	//-> 4 word  e addio...
	//

	uint16_t cfg_action[1]; // togli da qui// tipo messaggio scambiato
	// azione di configurazione
	// FC standard modbus
	//  0-16
	// 0-F 1 nibble

	uint16_t cfg_data[1]; //???  da togliere è la raccolta di se stessa
	// dati di configurazione
	// 0 a 800 registri come da standard modbus

	uint16_t msg_type[1]; //???  roba di messaggi tira via
	// tipo messaggio trasferito
	// registri e basta

	uint16_t rssi_level[1]; // messaggio ricevuto
	// livello del segnale radio ricevuto dall'IOD
	// 1 byte

	uint16_t bms_reaction[255]; // dentro sezione messaggi 
								//via da qui
								// non è un po grande? non mi è chiaro ancora bene a cosa serve..
							   // codice di reazione del sistema alle richieste da parte CMS
							   //- tabella di codici per ciascun PLC che gestisce uno o più bus con IOD
							   //- va definita sui PLC quando si creano le funzioni di reazione(es accendere luce temporizzata per illuminare un target, avviare filmato, etc.)
							   //- va memorizzata sul CMS(o sulla APP, da ragionare) e passata all'IOD via bluetooth
							   // 255 word per resistuire ampia gamma di reazioni
};

int init_str_iod_factoryDefault(struct str_iod *Fstruttura_iod);
int init_str_iod_LoadFromFlash(struct str_iod *Fstruttura_iod);


int init_hw_iod(void);

#endif
