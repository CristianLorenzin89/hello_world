/*
 * Copyright (c) 2021 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: LicenseRef-Nordic-5-Clause
 */
// iod beacon mode
#include <zephyr/kernel.h>
#include <zephyr/types.h>
#include <zephyr/sys/printk.h>

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/addr.h>

#include <zephyr/bluetooth/controller.h>
#include <zephyr/settings/settings.h>
#include <stddef.h>
#include <errno.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/sys/byteorder.h>
#include "iod.h"
#include "iod_modbus_rtu.h"

// C:\ncs\v2.2.0\zephyr\subsys\bluetooth\host

#define NON_CONNECTABLE_ADV_IDX 0
#define NON_CONNECTABLE_DEVICE_NAME "iodModbus"

// demo ble
static uint8_t milani_system_prefix[8];
// fine demo ble

extern struct str_iod iod;

static struct bt_le_ext_adv *ext_adv[CONFIG_BT_EXT_ADV_MAX_ADV_SET];
static const struct bt_le_adv_param *non_connectable_adv_param =
    BT_LE_ADV_PARAM(BT_LE_ADV_OPT_USE_NAME, // forza uso nome alla fine dell'advertising //
                    0x140,                  // 200 ms      // che strano modo.. 20=14 con 0 in coda.. bah
                    0x190,                  //      250 ms
                    NULL);

uint8_t AdvertisingBleTx[29];
static struct bt_data non_connectable_data[] = {
    // questi sono i miei dati ad esclusione del nome, aggiungo sotto campi
    /*BT_DATA(BT_DATA_MANUFACTURER_DATA, iod.milani_system_prefix, sizeof(iod.milani_system_prefix)),
    BT_DATA(BT_DATA_MANUFACTURER_DATA, iod.msg_type, sizeof(iod.msg_type)),
    BT_DATA(BT_DATA_MANUFACTURER_DATA, iod.bms_busid, sizeof(iod.bms_busid)),
    BT_DATA(BT_DATA_MANUFACTURER_DATA, iod.iod_busaddr, sizeof(iod.iod_busaddr))*/
    BT_DATA(BT_DATA_MANUFACTURER_DATA, AdvertisingBleTx, sizeof(AdvertisingBleTx))

};

////////////////// parte di ricezione

static bool eir_found(struct bt_data *data, void *user_data)
{

    bt_addr_le_t *addr = user_data;
    int i;

    // printk("[AD]: %u data_len %u\n", data->type, data->data_len);

    switch (data->type)
    {

    case BT_DATA_NAME_COMPLETE:
    {
        // non mi è chiaro xk non esce +
        /*	printk("stepx 2 ");
            printk("BT_DATA_NAME_COMPLETE \n");
            printk("nome dev %s \n", data->data);
            bytecpy(nomeBT, data->data, data->data_len);*/
    }
    break;
    case BT_DATA_MANUFACTURER_DATA:
    {

        // 666	printk("stepx 3 ");
        // 666printk("xxxxx \n");
        // 666printk("BT_DATA_MANUFACTURER_DATA \n");
        int sizeData = sizeof(data->data);
        // 666printk("sizeof(data->data) %d\n", sizeData);
        // 666printk("attributo \n");
        // 666printk("tipo %d\n", data->type);
        // 666printk("lunghezza %d \n", data->data_len);
        // 666printk("valore campo: ");

        for (int i = 0; i < data->data_len; i++)
        {
            printk(" %x ", data->data[i]);
        }
        printk("\n");
    }
    break;
    }

    return true;
}

static void device_found(const bt_addr_le_t *addr, int8_t rssi, uint8_t type,
                         struct net_buf_simple *ad)
{
    char dev[BT_ADDR_LE_STR_LEN];

    bt_addr_le_to_str(addr, dev, sizeof(dev));

    /*char nome[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    char ref[8] = {'M', 'i', 'l', 'a', 'n', 'i', 'A', 'A'};
    for (size_t i = 0; i < 8; i++)
    {
        nome[i] = ad->data[i + 2];
    }*/

    bytecpy(milani_system_prefix, ad->data + 2, 8);
    bool lres = true;

    for (size_t i = 0; i < ARRAY_SIZE(milani_system_prefix) - 2; i = i + 2)
    {
        uint16_t L1, L2;
        L1 = (uint16_t)(milani_system_prefix[i]);
        L2 = (uint16_t)(milani_system_prefix[i + 1]);
        uint16_t ricevuto = (L1 << 8) | (L2);
        uint16_t ref = iod.milani_system_prefix[i / 2];
        if (ricevuto != ref)
        {
            lres = false;
            break;
        }
    }

    if (lres)
    {
        DeserializzaBLEtoIOD(ad);
        // deserializza ble to iod--> hai su IOD, quindi puoi mandare sul bus       
       // update_modbus_register();  

       // bt_data_parse(ad, eir_found, (void *)addr); // se mai servisse ,tengo 
    }
}

static void start_scan(void)
{
    int err;
    struct bt_le_scan_param scan_param = {
        .type = BT_LE_SCAN_TYPE_ACTIVE,
        .options = BT_LE_SCAN_OPT_NONE,
        .interval = BT_GAP_SCAN_FAST_INTERVAL,
        .window = BT_GAP_SCAN_FAST_WINDOW,
    };
    err = bt_le_scan_start(&scan_param, device_found);
    if (err)
    {

        return;
    }
}
/////////////////fine parte ricezione

static void adv_connected_cb(struct bt_le_ext_adv *adv,
                             struct bt_le_ext_adv_connected_info *info)
{
    printk("Advertiser[%d] %p connected conn %p\n", bt_le_ext_adv_get_index(adv),
           adv, info->conn);
    // eventuale call back, da capire se mi serve altrimenti via
}

static const struct bt_le_ext_adv_cb adv_cb = {
    .connected = adv_connected_cb};

static int advertising_set_create(struct bt_le_ext_adv **adv,
                                  const struct bt_le_adv_param *param,
                                  const struct bt_data *ad, size_t ad_len)
{
    int err;
    struct bt_le_ext_adv *adv_set;

    err = bt_le_ext_adv_create(param, &adv_cb,
                               adv);
    if (err)
    {
        return err;
    }

    adv_set = *adv;

    printk("Created adv: %p\n", adv_set);

    err = bt_le_ext_adv_set_data(adv_set, ad, ad_len, NULL, 0);
    if (err)
    {
        printk("Failed to set advertising data (err %d)\n", err);
        return err;
    }

    return bt_le_ext_adv_start(adv_set, BT_LE_EXT_ADV_START_DEFAULT);
    // return 0;
}

void serializzaIODtoBLE()
{
    // milani_system_prefix
    // iod.msg_type
    // iod.bms_busid
    // iod.iod_busaddr
    int i, j, k;
    int lcounter = 0;

    for (i = 0; i < ARRAY_SIZE(iod.milani_system_prefix); i++)
    {
        uint8_t Lsb = (uint8_t)(iod.milani_system_prefix[i]);
        uint8_t Msb = (uint8_t)(arithmetic_shift_right(iod.milani_system_prefix[i], 8));
        AdvertisingBleTx[lcounter] = Msb;
        lcounter++;
        AdvertisingBleTx[lcounter] = Lsb;
        lcounter++;
    }

    for (i = 0; i < ARRAY_SIZE(iod.msg_type); i++)
    {
        uint8_t Lsb = (uint8_t)(iod.msg_type[i]);
        uint8_t Msb = (uint8_t)(arithmetic_shift_right(iod.msg_type[i], 8));
        AdvertisingBleTx[lcounter] = Msb;
        lcounter++;
        AdvertisingBleTx[lcounter] = Lsb;
        lcounter++;
    }

    for (i = 0; i < ARRAY_SIZE(iod.bms_busid); i++)
    {
        uint8_t Lsb = (uint8_t)(iod.bms_busid[i]);
        uint8_t Msb = (uint8_t)(arithmetic_shift_right(iod.bms_busid[i], 8));
        AdvertisingBleTx[lcounter] = Msb;
        lcounter++;
        AdvertisingBleTx[lcounter] = Lsb;
        lcounter++;
    }

    for (i = 0; i < ARRAY_SIZE(iod.iod_busaddr); i++)
    {
        uint8_t Lsb = (uint8_t)(iod.iod_busaddr[i]);
        uint8_t Msb = (uint8_t)(arithmetic_shift_right(iod.iod_busaddr[i], 8));
        AdvertisingBleTx[lcounter] = Msb;
        lcounter++;
        AdvertisingBleTx[lcounter] = Lsb;
        lcounter++;
    }
}

void DeserializzaBLEtoIOD(struct net_buf_simple *adRec)
{
    /*
        int i, j, k;

        int lcounter = 0;

        lcounter = 2;
        // non voglio ridefinire i primi 8 byte in quanto sono identificativo ble device
        // devo usarlo solo per filtrare
        for (i = 0; i < ARRAY_SIZE(iod.milani_system_prefix); i++)
        {
            uint16_t MsB = adRec->data[lcounter] & 0xff;
            lcounter++;
            uint16_t LsB = adRec->data[lcounter] & 0xff;
            lcounter++;
            uint16_t val = MsB << 8 | LsB;
            iod.milani_system_prefix[i] = val;
        }

        for (i = 0; i < ARRAY_SIZE(iod.msg_type); i++)
        {
            uint16_t MsB = adRec->data[lcounter] & 0xff;
            lcounter++;
            uint16_t LsB = adRec->data[lcounter] & 0xff;
            lcounter++;
            uint16_t val = MsB << 8 | LsB;
            iod.msg_type[i] = val;
        }
        */
}

static int non_connectable_adv_create(void)
{
    int err;

    err = bt_set_name(NON_CONNECTABLE_DEVICE_NAME);
    if (err)
    {
        printk("Failed to set device name (err %d)\n", err);
        return err;
    }

    serializzaIODtoBLE();

    err = advertising_set_create(&ext_adv[NON_CONNECTABLE_ADV_IDX], non_connectable_adv_param,
                                 non_connectable_data, ARRAY_SIZE(non_connectable_data));
    if (err)
    {
        printk("Failed to create a non-connectable advertising set (err %d)\n", err);
    }

    return err;
}

int initBLE(void)
{

    int err = 0;
    printk("parto\n");

    // bt_identity
    err = err + bt_enable(NULL);
    if (err)
    {
        printk("Bluetooth init failed (err %d)\n", err);
        return err;
    }

    printk("Bluetooth initialized\n");

    err = err + non_connectable_adv_create();
    if (err)
    {
        return err;
    }

    printk("Non-connectable advertising started\n");
    printk("a\n");
    start_scan();
    return err;
}

int DisableUpdateEnableBLE(void)
{
    // dismissed
    uint16_t *dummy = non_connectable_data[3].data; // puntatore ad array di unint16 o unint16 stesso

    dummy[0] = dummy[0] + 1;
    non_connectable_data[3].data = dummy;

    int lres = 0;
    printk("%d", lres);

    bt_le_ext_adv_stop(ext_adv[0]);

    int err;
    struct bt_le_ext_adv *adv_set;

    if (err)
    {
        return err;
    }

    adv_set = ext_adv[0];

    printk("Created adv: %p\n", adv_set);

    err = bt_le_ext_adv_set_data(adv_set, non_connectable_data, ARRAY_SIZE(non_connectable_data), NULL, 0);
    if (err)
    {
        printk("Failed to set advertising data (err %d)\n", err);
        return err;
    }

    return bt_le_ext_adv_start(adv_set, BT_LE_EXT_ADV_START_DEFAULT);
}

int UpdateBLE(void)
{
    iod.milani_system_prefix[3] = iod.milani_system_prefix[3] + 1;  // hreg 3    
    iod.msg_type[0] = iod.msg_type[0] + 1;  // ok  // hreg 18
    iod.bms_busid[0] = iod.bms_busid[0] + 1; //15  // hreg5
    iod.iod_busaddr[0] = iod.iod_busaddr[0]+1; //31  //hreg4

    serializzaIODtoBLE();
    update_modbus_register();  

    int err;
    struct bt_le_ext_adv *adv_set;

    if (err)
    {
        return err;
    }

    adv_set = ext_adv[0];
    printk("Created adv: %p\n", adv_set);
    bt_le_ext_adv_stop(ext_adv[0]);  
    err = bt_le_ext_adv_set_data(adv_set, non_connectable_data, ARRAY_SIZE(non_connectable_data), NULL, 0);
    return err + bt_le_ext_adv_start(adv_set, BT_LE_EXT_ADV_START_DEFAULT); 
    
}
