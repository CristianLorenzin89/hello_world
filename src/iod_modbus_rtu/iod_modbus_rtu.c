#include "iod_modbus_rtu.h"

#include <zephyr/kernel.h>
#include <zephyr/sys/util.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/modbus/modbus.h>
#include <zephyr/usb/usb_device.h>
#include <string.h>
#include "iod.h"
#include "iod_modbus_rtu.h"
#include "iod_nvs.h" //!!!!! togliere, solo di test

#define MODBUS_NODE DT_COMPAT_GET_ANY_STATUS_OKAY(zephyr_modbus_serial)

const int ADDR_MILANI_SYSTEM_PREFIX = 0; // dimensione 4
const int ADDR_IOD_BUSADDR = 4;			 // dimensione 1
const int ADDR_BMS_BUSID = 5;			 // dimensione 1
const int ADDR_IOD_CONFIG_ID = 6;		 // dimensione 1
const int ADDR_TARGETOBJ_ID = 7;		 // dimensione 2
const int ADDR_APP_ID = 9;				 // dimensione 1
const int ADDR_TAG_ID = 10;				 // dimensione 2
const int ADDR_CFG_PASSWORD = 12;		 // dimensione 4
const int ADDR_CFG_ACTION = 16;			 // dimensione 1
const int ADDR_CFG_DATA = 17;			 // dimensione 1
const int ADDR_MSG_TYPE = 18;			 // dimensione 1
const int ADDR_RSSI_LEVEL = 19;			 // dimensione 1
const int ADDR_BMS_REACTION = 100;		 // dimensione 255

const int SIZE_H_REG = 512; // dimensione hreg 512

const int SIZE_MILANI_SYSTEM_PREFIX = 4; // dimensione 4
const int SIZE_IOD_BUSADDR = 1;			 // dimensione 1
const int SIZE_BMS_BUSID = 1;			 // dimensione 1
const int SIZE_IOD_CONFIG_ID = 1;		 // dimensione 1
const int SIZE_TARGETOBJ_ID = 2;		 // dimensione 2
const int SIZE_APP_ID = 1;				 // dimensione 1
const int SIZE_TAG_ID = 2;				 // dimensione 2
const int SIZE_CFG_PASSWORD = 4;		 // dimensione 4
const int SIZE_CFG_ACTION = 1;			 // dimensione 1
const int SIZE_CFG_DATA = 1;			 // dimensione 1
const int SIZE_MSG_TYPE = 1;			 // dimensione 1
const int SIZE_RSSI_LEVEL = 1;			 // dimensione 1
const int SIZE_BMS_REACTION = 255;		 // dimensione 255

extern const struct gpio_dt_spec led_dev[];
extern struct str_iod iod;

static uint16_t holding_reg[512];

static int coil_rd(uint16_t addr, bool *state)
{
	return 0;
}

static int coil_wr(uint16_t addr, bool state)
{

	gpio_pin_set_dt(&led_dev[0], state);
	gpio_pin_set_dt(&led_dev[1], state);
	gpio_pin_set_dt(&led_dev[2], state);

	return 0;
}

static int holding_reg_rd(uint16_t addr, uint16_t *reg)
{

	if (addr >= ARRAY_SIZE(holding_reg))
	{
		return -ENOTSUP;
	}

	*reg = holding_reg[addr];

	return 0;
}

/*static*/ int holding_reg_wr(uint16_t addr, uint16_t reg)
{
	holding_reg[addr] = reg;

	switch (addr)
	{
	case ADDR_MILANI_SYSTEM_PREFIX ... ADDR_MILANI_SYSTEM_PREFIX + ARRAY_SIZE(iod.milani_system_prefix) - 1: // 666 da qui in poi per tutto quello che è in struttura
	{																										 // in caso di write multiple register viene chiamata nvolte
		// bytecpy(iod.milani_system_prefix, holding_reg, sizeof(iod.milani_system_prefix));
		// mi pare poco efficente come approccio, copio a mano..
		iod.milani_system_prefix[addr - ADDR_MILANI_SYSTEM_PREFIX] = holding_reg[addr];
	}
	break;

	case ADDR_IOD_BUSADDR ... ADDR_IOD_BUSADDR + ARRAY_SIZE(iod.iod_busaddr) - 1:
	{
		iod.iod_busaddr[addr - ADDR_IOD_BUSADDR] = holding_reg[addr];
	}
	break;

	case ADDR_BMS_BUSID ... ADDR_BMS_BUSID + ARRAY_SIZE(iod.bms_busid) - 1:
	{
		iod.bms_busid[addr - ADDR_BMS_BUSID] = holding_reg[addr];
	}
	break;

	case ADDR_IOD_CONFIG_ID ... ADDR_IOD_CONFIG_ID + ARRAY_SIZE(iod.iod_config_id) - 1:
	{
		iod.iod_config_id[addr - ADDR_IOD_CONFIG_ID] = holding_reg[addr];
	}
	break;

	case ADDR_TARGETOBJ_ID ... ADDR_TARGETOBJ_ID + ARRAY_SIZE(iod.targetobj_id) - 1:
	{
		iod.targetobj_id[addr - ADDR_TARGETOBJ_ID] = holding_reg[addr];
	}
	break;

	case ADDR_APP_ID ... ADDR_APP_ID + ARRAY_SIZE(iod.app_id) - 1:
	{
		iod.app_id[addr - ADDR_APP_ID] = holding_reg[addr];
	}
	break;

	case ADDR_TAG_ID ... ADDR_TAG_ID + ARRAY_SIZE(iod.tag_id) - 1:
	{
		iod.tag_id[addr - ADDR_TAG_ID] = holding_reg[addr];
	}
	break;

	case ADDR_CFG_PASSWORD ... ADDR_CFG_PASSWORD + ARRAY_SIZE(iod.cfg_password) - 1:
	{
		iod.cfg_password[addr - ADDR_CFG_PASSWORD] = holding_reg[addr];
	}
	break;

	case ADDR_CFG_ACTION ... ADDR_CFG_ACTION + ARRAY_SIZE(iod.cfg_action) - 1:
	{
		iod.cfg_action[addr - ADDR_CFG_ACTION] = holding_reg[addr];
	}
	break;

	case ADDR_CFG_DATA ... ADDR_CFG_DATA + ARRAY_SIZE(iod.cfg_data) - 1:
	{
		iod.cfg_data[addr - ADDR_CFG_DATA] = holding_reg[addr];
	}
	break;

	case ADDR_MSG_TYPE ... ADDR_MSG_TYPE + ARRAY_SIZE(iod.msg_type) - 1:
	{
		iod.msg_type[addr - ADDR_MSG_TYPE] = holding_reg[addr];
	}
	break;

	case ADDR_RSSI_LEVEL ... ADDR_RSSI_LEVEL + ARRAY_SIZE(iod.rssi_level) - 1:
	{
		iod.rssi_level[addr - ADDR_RSSI_LEVEL] = holding_reg[addr];
	}
	break;

	case ADDR_BMS_REACTION ... ADDR_BMS_REACTION + ARRAY_SIZE(iod.bms_reaction) - 1:
	{
		iod.bms_reaction[addr - ADDR_BMS_REACTION] = holding_reg[addr];
	}
	break;

	case 511:
	{
		if (holding_reg[511] == 1)
		{
			if (storeDataIntoFlash() == 0)
			{
				holding_reg[511] = 0;
			}
			else
			{
				holding_reg[511] = -1;
			}
		}
	}
	break;

	default:
		break;
	}

	return 0;
}

static struct modbus_user_callbacks mbs_cbs = {
	.coil_rd = coil_rd,
	.coil_wr = coil_wr,
	.holding_reg_rd = holding_reg_rd,
	.holding_reg_wr = holding_reg_wr,
};

static struct modbus_iface_param server_param = {
	.mode = MODBUS_MODE_RTU,
	.server = {
		.user_cb = &mbs_cbs,
		.unit_id = 10,
	},
	.serial = {
		.baud = 9600,
		.parity = UART_CFG_PARITY_NONE,
		.stop_bits_client = UART_CFG_STOP_BITS_1,
	},
};

static void _fillModbusRegisterStartUpDefault(void)
{
	int LcounterReg = 0;
	int LnumReg = sizeof(holding_reg) / sizeof(holding_reg[0]);
	int Llen = sizeof(iod.milani_system_prefix) / sizeof(iod.milani_system_prefix[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		if (ADDR_MILANI_SYSTEM_PREFIX + LcounterReg < LnumReg)
		{
			holding_reg[ADDR_MILANI_SYSTEM_PREFIX + LcounterReg] = iod.milani_system_prefix[i]; // 666 da qui in giu?
			LcounterReg++;
		}
	}

	LcounterReg = 0;
	Llen = sizeof(iod.iod_busaddr) / sizeof(iod.iod_busaddr[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		if (ADDR_IOD_BUSADDR + LcounterReg < LnumReg)
		{
			holding_reg[ADDR_IOD_BUSADDR + LcounterReg] = iod.iod_busaddr[i];
			LcounterReg++;
		}
	}

	LcounterReg = 0;
	Llen = sizeof(iod.bms_busid) / sizeof(iod.bms_busid[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		if (ADDR_BMS_BUSID + LcounterReg < LnumReg)
		{
			holding_reg[ADDR_BMS_BUSID + LcounterReg] = iod.bms_busid[i];
			LcounterReg++;
		}
	}

	LcounterReg = 0;
	Llen = sizeof(iod.iod_config_id) / sizeof(iod.iod_config_id[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		if (ADDR_IOD_CONFIG_ID + LcounterReg < LnumReg)
		{
			holding_reg[ADDR_IOD_CONFIG_ID + LcounterReg] = iod.iod_config_id[i];
			LcounterReg++;
		}
	}

	LcounterReg = 0;
	Llen = sizeof(iod.targetobj_id) / sizeof(iod.targetobj_id[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		if (ADDR_TARGETOBJ_ID + LcounterReg < LnumReg)
		{
			holding_reg[ADDR_TARGETOBJ_ID + LcounterReg] = iod.targetobj_id[i];
			LcounterReg++;
		}
	}

	LcounterReg = 0;
	Llen = sizeof(iod.app_id) / sizeof(iod.app_id[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		if (ADDR_APP_ID + LcounterReg < LnumReg)
		{
			holding_reg[ADDR_APP_ID + LcounterReg] = iod.app_id[i];
			LcounterReg++;
		}
	}

	LcounterReg = 0;
	Llen = sizeof(iod.tag_id) / sizeof(iod.tag_id[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		if (ADDR_TAG_ID + LcounterReg < LnumReg)
		{
			holding_reg[ADDR_TAG_ID + LcounterReg] = iod.tag_id[i];
			LcounterReg++;
		}
	}

	LcounterReg = 0;
	Llen = sizeof(iod.cfg_password) / sizeof(iod.cfg_password[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		if (ADDR_CFG_PASSWORD + LcounterReg < LnumReg)
		{
			holding_reg[ADDR_CFG_PASSWORD + LcounterReg] = iod.cfg_password[i];
			LcounterReg++;
		}
	}

	LcounterReg = 0;
	Llen = sizeof(iod.cfg_action) / sizeof(iod.cfg_action[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		if (ADDR_CFG_ACTION + LcounterReg < LnumReg)
		{
			holding_reg[ADDR_CFG_ACTION + LcounterReg] = iod.cfg_action[i];
			LcounterReg++;
		}
	}

	LcounterReg = 0;
	Llen = sizeof(iod.cfg_data) / sizeof(iod.cfg_data[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		if (ADDR_CFG_DATA + LcounterReg < LnumReg)
		{
			holding_reg[ADDR_CFG_DATA + LcounterReg] = iod.cfg_data[i];
			LcounterReg++;
		}
	}

	LcounterReg = 0;
	Llen = sizeof(iod.msg_type) / sizeof(iod.msg_type[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		if (ADDR_MSG_TYPE + LcounterReg < LnumReg)
		{
			holding_reg[ADDR_MSG_TYPE + LcounterReg] = iod.msg_type[i];
			LcounterReg++;
		}
	}

	LcounterReg = 0;
	Llen = sizeof(iod.rssi_level) / sizeof(iod.rssi_level[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		if (ADDR_RSSI_LEVEL + LcounterReg < LnumReg)
		{
			holding_reg[ADDR_RSSI_LEVEL + LcounterReg] = iod.rssi_level[i];
			LcounterReg++;
		}
	}

	LcounterReg = 0;
	Llen = sizeof(iod.bms_reaction) / sizeof(iod.bms_reaction[0]);
	for (size_t i = 0; i < Llen; i++)
	{
		if (ADDR_BMS_REACTION + LcounterReg < LnumReg)
		{
			holding_reg[ADDR_BMS_REACTION + LcounterReg] = iod.bms_reaction[i];
			LcounterReg++;
		}
	}
}

int init_modbus_slave(uint8_t LidSlave)
{
	const char iface_name[] = {DEVICE_DT_NAME(MODBUS_NODE)};
	int iface;
	iface = modbus_iface_get_by_name(iface_name);
	if (iface < 0)
	{
		return iface;
	}
	server_param.server.unit_id = LidSlave;

	int lres = modbus_init_server(iface, server_param);

	_fillModbusRegisterStartUpDefault();

	return lres;
}

void update_modbus_register()
{
	_fillModbusRegisterStartUpDefault();
}