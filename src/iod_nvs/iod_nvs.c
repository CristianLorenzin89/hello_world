

#include <zephyr/kernel.h>
#include <zephyr/sys/util.h>
#include <zephyr/drivers/gpio.h>

#include <string.h>
#include "iod.h"
#include "iod_modbus_rtu.h"
#include "iod_nvs.h"

//!!!!!!! capire quali puoi togliere

#include <zephyr/device.h>

#include <zephyr/drivers/flash.h>
#include <zephyr/storage/flash_map.h>
#include <zephyr/fs/nvs.h>

struct nvs_fs fs;

#define NVS_PARTITION storage_partition
#define NVS_PARTITION_DEVICE FIXED_PARTITION_DEVICE(NVS_PARTITION)
#define NVS_PARTITION_OFFSET FIXED_PARTITION_OFFSET(NVS_PARTITION)

#define ID_MILANI_SYSTEM_PREFIX 1
#define ID_IOD_BUSADDR 2
#define ID_BMS_BUSID 3
#define ID_IOD_CONFIG_ID 4
#define ID_TARGETOBJ_ID 5
#define ID_APP_ID 6
#define ID_TAG_ID 7
#define ID_CFG_PASSWORD 8
#define ID_CFG_ACTION 9
#define ID_CFG_DATA 10
#define ID_MSG_TYPE 11
#define ID_RSSI_LEVEL 12
#define ID_BMS_REACTION 13

extern struct str_iod iod;

uint16_t LFromFlash_storedData_milani_system_prefix[ARRAY_SIZE(iod.milani_system_prefix)];
uint16_t LFromFlash_storedData_iod_busaddr[ARRAY_SIZE(iod.iod_busaddr)];
uint16_t LFromFlash_storedData_bms_busid[ARRAY_SIZE(iod.bms_busid)];
uint16_t LFromFlash_storedData_iod_config_id[ARRAY_SIZE(iod.iod_config_id)];
uint16_t LFromFlash_storedData_targetobj_id[ARRAY_SIZE(iod.targetobj_id)];
uint16_t LFromFlash_storedData_app_id[ARRAY_SIZE(iod.app_id)];
uint16_t LFromFlash_storedData_tag_id[ARRAY_SIZE(iod.tag_id)];
uint16_t LFromFlash_storedData_cfg_password[ARRAY_SIZE(iod.cfg_password)];
uint16_t LFromFlash_storedData_cfg_action[ARRAY_SIZE(iod.cfg_action)];
uint16_t LFromFlash_storedData_cfg_data[ARRAY_SIZE(iod.cfg_data)];
uint16_t LFromFlash_storedData_msg_type[ARRAY_SIZE(iod.msg_type)];
uint16_t LFromFlash_storedData_rssi_level[ARRAY_SIZE(iod.rssi_level)];
uint16_t LFromFlash_storedData_bms_reaction[ARRAY_SIZE(iod.bms_reaction)];

int _initFs(void)
{
	struct flash_pages_info info;
	int rc = 0;
	int LresLoadFromFalsh = 0;

	fs.flash_device = NVS_PARTITION_DEVICE;
	if (!device_is_ready(fs.flash_device))
	{
		printk("Flash device %s is not ready\n", fs.flash_device->name);
		return -1;
	}
	fs.offset = NVS_PARTITION_OFFSET;
	rc = flash_get_page_info_by_offs(fs.flash_device, fs.offset, &info);
	if (rc)
	{
		LresLoadFromFalsh = LresLoadFromFalsh + rc;
		return LresLoadFromFalsh;
	}

	fs.sector_size = info.size;
	fs.sector_count = 3U;

	rc = nvs_mount(&fs);
	if (rc)
	{
		LresLoadFromFalsh = LresLoadFromFalsh + rc;
		return LresLoadFromFalsh;
	}

	// per pulire la flash
	/*	nvs_clear(&fs); //666

		rc = nvs_mount(&fs);
		if (rc)
		{
			LresLoadFromFalsh = LresLoadFromFalsh + rc;
			return LresLoadFromFalsh;
		}*/
}

int _loadOrStoreFromFlash(uint16_t fid, uint16_t *fFlashStoredData, uint16_t *fiodStructureData, int fSize)
{
	int rc = 0;
	int LsizeBuffByte = fSize * sizeof(uint16_t);
	rc = nvs_read(&fs, fid, fFlashStoredData, LsizeBuffByte);
	if (rc > 0)
	{
		// se trovo il campo lo carico nella struttura iod
		bytecpy(fiodStructureData, fFlashStoredData, LsizeBuffByte);
	}
	else
	{
		// se non trovo il campo in flash allora inizio a scrivere il campo di default. In questo modo il primo avvio è leggermente + lento ma poi sono sicuro che il campo è presente
		bytecpy(fFlashStoredData, fiodStructureData, LsizeBuffByte);
		// si lo so potrei copiare direttamente la struttura iod, mi piace cosi
		rc = nvs_write(&fs, fid, fFlashStoredData, LsizeBuffByte);
	}
	return rc - LsizeBuffByte;
}

int _StoreIntoFlash(uint16_t fid, uint16_t *fFlashStoredData, uint16_t *fiodStructureData, int fSize)
{
	int rc = 0;
	int LsizeBuffByte = fSize * sizeof(uint16_t);
	// copio in un array locale i data che ho nella struttura iod
	bytecpy(fFlashStoredData, fiodStructureData, LsizeBuffByte);
	// si lo so potrei copiare direttamente la struttura iod, mi piace cosi, e faccio uguale a loadorstore
	rc = nvs_write(&fs, fid, fFlashStoredData, LsizeBuffByte);
	return rc - LsizeBuffByte;
}

int loadDataFromFlash(void)
{
	int rc = 0;
	int LresLoadFromFalsh = 0;
	if (&fs.ready != true)
		LresLoadFromFalsh = _initFs();

	/*static struct flash_pages_info info;

	fs.flash_device = NVS_PARTITION_DEVICE;
	if (!device_is_ready(fs.flash_device))
	{
		printk("Flash device %s is not ready\n", fs.flash_device->name);
		return -1;
	}
	fs.offset = NVS_PARTITION_OFFSET;
	rc = flash_get_page_info_by_offs(fs.flash_device, fs.offset, &info);
	if (rc)
	{
		LresLoadFromFalsh = LresLoadFromFalsh + rc;
		return LresLoadFromFalsh;
	}

	fs.sector_size = info.size;
	fs.sector_count = 3U;

	rc = nvs_mount(&fs);
	if (rc)
	{
		LresLoadFromFalsh = LresLoadFromFalsh + rc;
		return LresLoadFromFalsh;
	}

	nvs_clear(&fs);

	rc = nvs_mount(&fs);
	if (rc)
	{
		LresLoadFromFalsh = LresLoadFromFalsh + rc;
		return LresLoadFromFalsh;
	}*/

	LresLoadFromFalsh = LresLoadFromFalsh + _loadOrStoreFromFlash(ID_MILANI_SYSTEM_PREFIX, LFromFlash_storedData_milani_system_prefix, iod.milani_system_prefix, ARRAY_SIZE(LFromFlash_storedData_milani_system_prefix));
	if (rc)
	{
		return LresLoadFromFalsh;
	}

	LresLoadFromFalsh = LresLoadFromFalsh + _loadOrStoreFromFlash(ID_IOD_BUSADDR, LFromFlash_storedData_iod_busaddr, iod.iod_busaddr, ARRAY_SIZE(LFromFlash_storedData_iod_busaddr));
	if (rc)
	{
		return LresLoadFromFalsh;
	}

	LresLoadFromFalsh = LresLoadFromFalsh + _loadOrStoreFromFlash(ID_BMS_BUSID, LFromFlash_storedData_bms_busid, iod.bms_busid, ARRAY_SIZE(LFromFlash_storedData_bms_busid));
	if (rc)
	{
		return LresLoadFromFalsh;
	}

	LresLoadFromFalsh = LresLoadFromFalsh + _loadOrStoreFromFlash(ID_IOD_CONFIG_ID, LFromFlash_storedData_iod_config_id, iod.iod_config_id, ARRAY_SIZE(LFromFlash_storedData_iod_config_id));
	if (rc)
	{
		return LresLoadFromFalsh;
	}

	LresLoadFromFalsh = LresLoadFromFalsh + _loadOrStoreFromFlash(ID_TARGETOBJ_ID, LFromFlash_storedData_targetobj_id, iod.targetobj_id, ARRAY_SIZE(LFromFlash_storedData_targetobj_id));
	if (rc)
	{
		return LresLoadFromFalsh;
	}

	LresLoadFromFalsh = LresLoadFromFalsh + _loadOrStoreFromFlash(ID_APP_ID, LFromFlash_storedData_app_id, iod.app_id, ARRAY_SIZE(LFromFlash_storedData_app_id)); //!
	if (rc)
	{
		return LresLoadFromFalsh;
	}

	LresLoadFromFalsh = LresLoadFromFalsh + _loadOrStoreFromFlash(ID_TAG_ID, LFromFlash_storedData_tag_id, iod.tag_id, ARRAY_SIZE(LFromFlash_storedData_tag_id));
	if (rc)
	{
		return LresLoadFromFalsh;
	}

	LresLoadFromFalsh = LresLoadFromFalsh + _loadOrStoreFromFlash(ID_CFG_PASSWORD, LFromFlash_storedData_cfg_password, iod.cfg_password, ARRAY_SIZE(LFromFlash_storedData_cfg_password));
	if (rc)
	{
		return LresLoadFromFalsh;
	}

	LresLoadFromFalsh = LresLoadFromFalsh + _loadOrStoreFromFlash(ID_CFG_ACTION, LFromFlash_storedData_cfg_action, iod.cfg_action, ARRAY_SIZE(LFromFlash_storedData_cfg_action));
	if (rc)
	{
		return LresLoadFromFalsh;
	}

	LresLoadFromFalsh = LresLoadFromFalsh + _loadOrStoreFromFlash(ID_CFG_DATA, LFromFlash_storedData_cfg_data, iod.cfg_data, ARRAY_SIZE(LFromFlash_storedData_cfg_data));
	if (rc)
	{
		return LresLoadFromFalsh;
	}

	LresLoadFromFalsh = LresLoadFromFalsh + _loadOrStoreFromFlash(ID_MSG_TYPE, LFromFlash_storedData_msg_type, iod.msg_type, ARRAY_SIZE(LFromFlash_storedData_msg_type));
	if (rc)
	{
		return LresLoadFromFalsh;
	}

	LresLoadFromFalsh = LresLoadFromFalsh + _loadOrStoreFromFlash(ID_RSSI_LEVEL, LFromFlash_storedData_rssi_level, iod.rssi_level, ARRAY_SIZE(LFromFlash_storedData_rssi_level));
	if (rc)
	{
		return LresLoadFromFalsh;
	}

	LresLoadFromFalsh = LresLoadFromFalsh + _loadOrStoreFromFlash(ID_BMS_REACTION, LFromFlash_storedData_bms_reaction, iod.bms_reaction, ARRAY_SIZE(LFromFlash_storedData_bms_reaction));
	if (rc)
	{
		return LresLoadFromFalsh;
	}

	// nvs_clear(&fs);
	// size_t spazio = nvs_calc_free_space(&fs);

	/*testato
	rimane da vedere se funziona se hai valori salvati diversi da quelli di default

*/
	/*rc = nvs_read(&fs, ID_MILANI_SYSTEM_PREFIX, &LFromFlash_storedData_milani_system_prefix, ARRAY_SIZE(LFromFlash_storedData_milani_system_prefix));
	if (rc > 0)
	{
		// se trovo il campo lo carico nella struttura iod
		bytecpy(iod.milani_system_prefix, LFromFlash_storedData_milani_system_prefix, sizeof(LFromFlash_storedData_milani_system_prefix));
	}
	else
	{
		// se non trovo il campo in flash allora inizio a scrivere il campo di default. In questo modo il primo avvio è leggermente + lento ma poi sono sicuro che il campo è presente
		bytecpy(LFromFlash_storedData_milani_system_prefix, iod.milani_system_prefix, sizeof(LFromFlash_storedData_milani_system_prefix));
		// si lo so potrei copiare direttamente la struttura iod, mi piace cosi
		(void)nvs_write(&fs, ID_MILANI_SYSTEM_PREFIX, &LFromFlash_storedData_milani_system_prefix, sizeof(LFromFlash_storedData_milani_system_prefix));
	}*/
}
int storeDataIntoFlash(void)
{
	int rc = 0;
	int LresStoreIntoFalsh = 0;
	if (&fs.ready != true)
		LresStoreIntoFalsh = _initFs();

	LresStoreIntoFalsh = LresStoreIntoFalsh + _StoreIntoFlash(ID_MILANI_SYSTEM_PREFIX, LFromFlash_storedData_milani_system_prefix, iod.milani_system_prefix, ARRAY_SIZE(LFromFlash_storedData_milani_system_prefix));
	if (rc)
	{
		return LresStoreIntoFalsh;
	}

	LresStoreIntoFalsh = LresStoreIntoFalsh + _StoreIntoFlash(ID_IOD_BUSADDR, LFromFlash_storedData_iod_busaddr, iod.iod_busaddr, ARRAY_SIZE(LFromFlash_storedData_iod_busaddr));
	if (rc)
	{
		return LresStoreIntoFalsh;
	}

	LresStoreIntoFalsh = LresStoreIntoFalsh + _StoreIntoFlash(ID_BMS_BUSID, LFromFlash_storedData_bms_busid, iod.bms_busid, ARRAY_SIZE(LFromFlash_storedData_bms_busid));
	if (rc)
	{
		return LresStoreIntoFalsh;
	}

	LresStoreIntoFalsh = LresStoreIntoFalsh + _StoreIntoFlash(ID_IOD_CONFIG_ID, LFromFlash_storedData_iod_config_id, iod.iod_config_id, ARRAY_SIZE(LFromFlash_storedData_iod_config_id));
	if (rc)
	{
		return LresStoreIntoFalsh;
	}

	LresStoreIntoFalsh = LresStoreIntoFalsh + _StoreIntoFlash(ID_TARGETOBJ_ID, LFromFlash_storedData_targetobj_id, iod.targetobj_id, ARRAY_SIZE(LFromFlash_storedData_targetobj_id));
	if (rc)
	{
		return LresStoreIntoFalsh;
	}

	LresStoreIntoFalsh = LresStoreIntoFalsh + _StoreIntoFlash(ID_APP_ID, LFromFlash_storedData_app_id, iod.app_id, ARRAY_SIZE(LFromFlash_storedData_app_id)); //!
	if (rc)
	{
		return LresStoreIntoFalsh;
	}

	LresStoreIntoFalsh = LresStoreIntoFalsh + _StoreIntoFlash(ID_TAG_ID, LFromFlash_storedData_tag_id, iod.tag_id, ARRAY_SIZE(LFromFlash_storedData_tag_id));
	if (rc)
	{
		return LresStoreIntoFalsh;
	}

	LresStoreIntoFalsh = LresStoreIntoFalsh + _StoreIntoFlash(ID_CFG_PASSWORD, LFromFlash_storedData_cfg_password, iod.cfg_password, ARRAY_SIZE(LFromFlash_storedData_cfg_password));
	if (rc)
	{
		return LresStoreIntoFalsh;
	}

	LresStoreIntoFalsh = LresStoreIntoFalsh + _StoreIntoFlash(ID_CFG_ACTION, LFromFlash_storedData_cfg_action, iod.cfg_action, ARRAY_SIZE(LFromFlash_storedData_cfg_action));
	if (rc)
	{
		return LresStoreIntoFalsh;
	}

	LresStoreIntoFalsh = LresStoreIntoFalsh + _StoreIntoFlash(ID_CFG_DATA, LFromFlash_storedData_cfg_data, iod.cfg_data, ARRAY_SIZE(LFromFlash_storedData_cfg_data));
	if (rc)
	{
		return LresStoreIntoFalsh;
	}

	LresStoreIntoFalsh = LresStoreIntoFalsh + _StoreIntoFlash(ID_MSG_TYPE, LFromFlash_storedData_msg_type, iod.msg_type, ARRAY_SIZE(LFromFlash_storedData_msg_type));
	if (rc)
	{
		return LresStoreIntoFalsh;
	}

	LresStoreIntoFalsh = LresStoreIntoFalsh + _StoreIntoFlash(ID_RSSI_LEVEL, LFromFlash_storedData_rssi_level, iod.rssi_level, ARRAY_SIZE(LFromFlash_storedData_rssi_level));
	if (rc)
	{
		return LresStoreIntoFalsh;
	}

	LresStoreIntoFalsh = LresStoreIntoFalsh + _StoreIntoFlash(ID_BMS_REACTION, LFromFlash_storedData_bms_reaction, iod.bms_reaction, ARRAY_SIZE(LFromFlash_storedData_bms_reaction));
	if (rc)
	{
		return LresStoreIntoFalsh;
	}
}
