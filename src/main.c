/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include "iod.h"
#include "iod_nvs.h"
#include "iod_modbus_rtu.h"
#include "iod_ble.h"
#include <zephyr/drivers/gpio.h>

extern const struct gpio_dt_spec led_dev[];
extern struct str_iod iod;

#define SLEEP_TIME_MS 100
#define SW0_NODE DT_ALIAS(sw0)
static const struct gpio_dt_spec button = GPIO_DT_SPEC_GET(SW0_NODE, gpios);

int init()
{
	int lres = 0;
	lres = init_str_iod_factoryDefault(&iod);
	lres = lres + init_str_iod_LoadFromFlash(&iod);
	lres = lres + init_hw_iod();
	lres = lres + init_modbus_slave(16);
	lres = lres + initBLE();
	printk("tutto ok");
	return lres;
}

void main(void)
{
	int lres = init();

	int zzz = 1;
	bool aggiornaCampo = true;

	/*while (zzz == 1)
	{
		k_msleep(SLEEP_TIME_MS);
	}*/

	gpio_pin_set_dt(&led_dev[0], true);

	bool val = false;
	bool dummy = false;
	int ret = gpio_pin_configure_dt(&button, GPIO_INPUT);
	int zz = 1;
	zz = 2;
	zz = 1;
	while (zz == 1)
	{

		val = gpio_pin_get_dt(&button);
		gpio_pin_set_dt(&led_dev[2], val);
		gpio_pin_set_dt(&led_dev[0], !val);
		gpio_pin_set_dt(&led_dev[1], dummy);
		if (aggiornaCampo)
		{
			int lres = UpdateBLE();
		}
		else
		{
			lres = 0;
		}

		printk("%d", lres);
		dummy = !dummy;
		k_msleep(10000);
	}
}
